//
//  main.m
//  Translater
//
//  Created by Vitalii Todorovych on 17.06.15.
//  Copyright (c) 2015 Vitalii Todorovych. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
